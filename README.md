# mms2file

Parse MMSd MMS and store to `~/Documents/MMS`.
Heavily based on [mms2mail](https://gitea.geodock.egeo.net.eu.org/Public/mms2mail).

## installation

### dependency

 - python3
 - python3-watchdog (sudo apt install python3-watchdog)
 - python-messaging (pip3 install python-messaging)

### setup
Install the dependency and mms2file:
```
sudo apt install python3-watchdog
pip3 install --user python-messaging

mkdir -p ~/.local/bin
cp mms2file ~/.local/bin
```
To enable the daemon mode in systemd user :
```
mkdir -p ~/.config/systemd/user
cp mms2file.service ~/.config/systemd/user/
systemctl --user daemon-reload
systemctl --user enable mms2file
systemctl --user start mms2file
```

## usage

```
mms2file [-h] [-d [{dbus,filesystem}] | -f FILES [FILES ...]] [--delete] [--force-read]

optional arguments:
  -h, --help            show this help message and exit
  -d [{dbus,filesystem}], --daemon [{dbus,filesystem}]
                        Use dbus signal from mmsd by default but can also watch mmsd storage folder (useful for mmsd < 1.0)
  -f FILES [FILES ...], --file FILES [FILES ...]
                        Parse specified mms files and quit
  --delete              Ask mmsd to delete the converted MMS
  --force-read          Force conversion even if MMS is marked as read
```
